//get user details
let fullName = document.querySelector("#profile-name")
let email = document.querySelector("#profile-email")
let mobileNo = document.querySelector("#profile-mobile")

//syntax: localStorage.getItem(<token>) - This will return the value of the key from our localStorage.
let token = localStorage.getItem('token');

//console.log(token);

fetch('http:localhost:4000/users/getUserDetails', {

        headers: {
            'Authorization': `Bearer ${token}`
        }

    })
    .then(res => res.json())
    .then(data => {

        //console.log(data.mobileNo);

        fullName.innerHTML = `${data.firstName} ${data.lastName}`
        email.innerHTML = `Email: ${data.email}`
        mobileNo.innerHTML = `Mobile: ${data.mobileNo}`

    })